/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/11 15:53:20 by flmarsil          #+#    #+#             */
/*   Updated: 2020/02/05 12:21:27 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libpf.h"

char	*pf_strnew(size_t size, int c)
{
	char *ret;

	if (!(ret = malloc(sizeof(char) * size + 1)))
		return (NULL);
	pf_memset(ret, c, size);
	ret[size] = '\0';
	return (ret);
}
