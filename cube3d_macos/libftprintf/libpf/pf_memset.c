/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/05 08:49:42 by flmarsil          #+#    #+#             */
/*   Updated: 2020/02/05 12:21:06 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libpf.h"

void	*pf_memset(void *b, int c, size_t len)
{
	size_t i;

	i = -1;
	while (len--)
		((unsigned char *)b)[++i] = (unsigned char)c;
	return (b);
}
