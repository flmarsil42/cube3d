/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 12:50:12 by flmarsil          #+#    #+#             */
/*   Updated: 2020/02/05 12:21:20 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libpf.h"

char	*pf_strjoin(char const *s1, char const *s2, int f)
{
	char	*ret;
	size_t	size;
	size_t	i;

	if (!s1 || !s2)
		return (NULL);
	size = (pf_strlen(s1) + pf_strlen(s2));
	if (!(ret = malloc(sizeof(char) * size + 1)))
		return (NULL);
	i = -1;
	while (*s1)
		ret[++i] = *s1++;
	while (*s2)
		ret[++i] = *s2++;
	ret[++i] = '\0';
	if ((f == 1 || f == 3) && *s1)
		free((char*)s1);
	if ((f == 2 || f == 3) && *s2)
		free((char*)s2);
	return (ret);
}
