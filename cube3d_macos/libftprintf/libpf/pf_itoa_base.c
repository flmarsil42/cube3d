/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/06 17:44:17 by flmarsil          #+#    #+#             */
/*   Updated: 2020/02/05 12:21:01 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libpf.h"

char	*pf_itoa_base(unsigned int value, char *base_to)
{
	char			*tab;
	int				cpt;
	unsigned int	tmp;
	unsigned int	base_len;

	base_len = pf_strlen(base_to);
	cpt = (value < 0) ? 2 : 1;
	tmp = (value < 0) ? -value : value;
	while (tmp >= base_len && (tmp /= base_len))
		++cpt;
	tmp = (value < 0) ? -value : value;
	if (!(tab = malloc(sizeof(char) * cpt + 1)))
		return (NULL);
	if (value < 0)
		tab[0] = '-';
	tab[cpt] = '\0';
	while (tmp >= base_len)
	{
		--cpt;
		tab[cpt] = base_to[tmp % base_len];
		tmp /= base_len;
	}
	tab[--cpt] = base_to[tmp % base_len];
	return (tab);
}
