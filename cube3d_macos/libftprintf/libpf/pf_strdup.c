/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/04 15:18:57 by flmarsil          #+#    #+#             */
/*   Updated: 2020/02/05 12:21:17 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libpf.h"

char	*pf_strdup(const char *s1)
{
	int		i;
	int		size;
	char	*dst;

	i = 0;
	size = pf_strlen(s1);
	if (!(dst = malloc(sizeof(char) * size + 1)))
		return (NULL);
	while (s1[i])
	{
		dst[i] = s1[i];
		i++;
	}
	dst[i] = '\0';
	return (dst);
}
