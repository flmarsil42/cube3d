/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 12:41:30 by flmarsil          #+#    #+#             */
/*   Updated: 2020/02/05 12:22:22 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

void	print_c(va_list av, t_elem *e)
{
	if (isnull(av, e) == 1 && ++e->count)
		write(1, "", 1);
	else if (!pf_strlen(e->ret))
	{
		e->final_len += final_check(e);
		(e->flag == -1 && ++e->count) ? write(1, "", 1) : 0;
		pf_putstr_fd(e->ret, 1);
		(e->flag != -1 && ++e->count) ? write(1, "", 1) : 0;
		free(e->ret);
	}
	else
	{
		e->final_len += final_check(e);
		pf_putstr_fd(e->ret, 1);
		free(e->ret);
	}
}

int		check_line(va_list av, char const *s, t_elem *e)
{
	while (s[++(e->i)])
	{
		if (s[e->i] != '%' && ++e->count)
			write(1, &s[e->i], 1);
		else
		{
			init_elem(e);
			++(e->i);
			if (fill_elem(av, e, s) == 1)
			{
				e->final_len += final_check(e);
				pf_putstr_fd(e->ret, 1);
				free(e->ret);
			}
			else if (s[e->i] == 'c')
				print_c(av, e);
			else
				return (-1);
		}
	}
	return (e->count + e->final_len);
}

int		ft_printf(const char *s, ...)
{
	va_list		av;
	t_elem		e;

	e.i = -1;
	e.count = 0;
	e.final_len = 0;
	va_start(av, s);
	e.final_len = check_line(av, s, &e);
	va_end(av);
	return (e.final_len);
}
