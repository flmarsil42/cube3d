/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transfo.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 13:20:43 by flmarsil          #+#    #+#             */
/*   Updated: 2020/02/05 12:22:33 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_printf.h"

int		string_precision(t_elem *e)
{
	char	*tmp;

	if (!(tmp = pf_substr(e->ret, 0, e->preci)))
		return (-1);
	free(e->ret);
	e->ret = tmp;
	return (1);
}

int		diuxxp_precision(t_elem *e)
{
	int		x;
	int		len_ret;
	char	*tmp;

	x = 0;
	tmp = NULL;
	len_ret = pf_strlen(e->ret);
	if (e->preci > len_ret - e->r_neg && e->zero > 0)
	{
		if (!(tmp = pf_calloc(sizeof(char *), e->preci + e->r_neg + 1)))
			return (-1);
		(e->r_neg == 1) ? tmp[x++] = '-' : 0;
		while (e->preci-- > (len_ret - e->r_neg))
			tmp[x++] = '0';
		tmp[x] = '\0';
		if (!(tmp = pf_strjoin(tmp, &e->ret[e->r_neg], 1)))
			return (-1);
		free(e->ret);
		e->ret = tmp;
		e->zero = 0;
	}
	return (1);
}

int		flag_zero(t_elem *e)
{
	char	*tmp;
	int		x;
	int		len_ret;

	x = 0;
	len_ret = pf_strlen(e->ret);
	if (!(tmp = pf_calloc(sizeof(char), e->width + e->r_neg + 1)))
		return (-1);
	(e->r_neg == 1) ? tmp[x++] = '-' : 0;
	while (e->width-- > len_ret)
		tmp[x++] = '0';
	tmp[x] = '\0';
	if (!(tmp = pf_strjoin(tmp, &e->ret[e->r_neg], 1)))
		return (-1);
	free(e->ret);
	e->ret = tmp;
	return (1);
}

int		dash_width(t_elem *e)
{
	char	*tmp;
	char	*tmp2;
	int		x;
	int		len_ret;

	x = 0;
	len_ret = pf_strlen(e->ret);
	if (!(tmp = pf_calloc(sizeof(char), (e->width + 1))))
		return (-1);
	e->width -= (e->type == 'c' && len_ret == 0) ? 1 : 0;
	while (e->width-- > len_ret)
		tmp[x++] = ' ';
	tmp[x] = '\0';
	tmp2 = tmp;
	if (!(tmp = (e->flag > -1) ? pf_strjoin(tmp, e->ret, 3)
		: pf_strjoin(e->ret, tmp, 3)))
		return (-1);
	free(e->ret);
	e->ret = tmp;
	free(tmp2);
	return (1);
}

int		final_check(t_elem *e)
{
	final_conversion(e);
	(e->type == 's' && e->preci >= 0) ? string_precision(e) : 0;
	(pf_strchr("diuxXp", e->type)) ? diuxxp_precision(e) : 0;
	if (e->zero == 1 && pf_strchr("diuxXcs%%", e->type))
		flag_zero(e);
	else if (e->width)
		dash_width(e);
	return (pf_strlen(e->ret));
}
