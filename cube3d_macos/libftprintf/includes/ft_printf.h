/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 13:56:18 by flmarsil          #+#    #+#             */
/*   Updated: 2020/02/05 12:20:24 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libpf.h"
# include <stdarg.h>

typedef struct		s_elem
{
	int				i;
	int				c;
	int				flag;
	int				width;
	int				preci;
	char			type;
	char			*ret;
	int				r_neg;
	int				p_neg;
	int				w_neg;
	int				zero;
	int				count;
	int				final_len;
}					t_elem;

int					ft_printf(char const *s, ...);

void				check_flag(t_elem *e, char const *s);
void				check_width(va_list av, t_elem *e, char const *s);
void				check_precision(va_list av, t_elem *e, char const *s);
int					check_type(va_list av, t_elem *e, char const *s);
int					check_line(va_list av, char const *s, t_elem *e);
int					isnull(va_list av, t_elem *e);

void				final_conversion(t_elem *e);
int					fill_elem(va_list av, t_elem *e, char const *s);
char				*convert_x(va_list av, char c, t_elem *e);
void				ft_except(t_elem *e);
void				init_elem(t_elem *e);

int					final_check(t_elem *e);
int					diuxxp_precision(t_elem *e);
int					string_precision(t_elem *e);
int					flag_zero(t_elem *e);
int					dash_width(t_elem *e);

#endif
