# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/11 14:47:38 by flmarsil          #+#    #+#              #
#    Updated: 2020/03/11 17:30:07 by flmarsil         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME				=	Cub3D

CC					=	gcc
CFLAGS				=	-Wall -Wextra -Werror -g3 -fsanitize=address
CXXFLAGS			=	-framework OpenGL -framework AppKit
RM					=	rm -rf

OPT					=	all Cub3D bonus
# ARG					=	$(findstring $(firstword $(MAKECMDGOALS)), $(OPT))

DIR_FT				=	./libft
DIR_PRINTF			=	./libftprintf
DIR_MLX				=	./minilibx
LIB_FT				=	-L$(DIR_FT) -lft
LIB_PRINTF			=	-L$(DIR_PRINTF) -lftprintf
LIB_MLX				=	-L$(DIR_MLX) -lmlx
LIBS				=	-lXext -lX11

DIR_INCS			=	./includes
DIR_BASIC			=	./srcs
# DIR_BONUS			=	./srcs/bonus

# ifeq ($(ARG), bonus)
# DIR_SRCS			=	$(DIR_BONUS)
# INCS				=	$(DIR_INCS)/cub3d_bonus.h
# else
# DIR_SRCS			=	$(DIR_BASIC)
# INCS				=	$(DIR_INCS)/cub3d.h
# endif

SRCS				=	$(addprefix srcs/, cube3d.c colors.c map.c parsing.c player.c \
					resolution.c size_tabmap.c sprites.c textures.c utils.c close.c draw_sprite.c draw.c \
					init_player.c keyboard.c load_textures.c move.c get_next_line.c get_next_line_utils.c \
					raycasting_sprites.c raycasting.c window.c screenshot.c len_security.c)

OBJS				=	$(SRCS:.c=.o)

all					:	$(NAME)

bonus				:	$(NAME)

$(NAME)				:	$(OBJS) #$(INCS)
						$(MAKE) -C $(DIR_FT)
						$(MAKE) -C $(DIR_PRINTF)
						$(MAKE) -C $(DIR_MLX)
						$(CC) $(CFLAGS) $(CXXFLAGS) $(OBJS) -o $(NAME) $(LIB_FT) $(LIB_PRINTF) $(LIB_MLX)

clean				:
						$(RM) $(DIR_BASIC)/*.o
						# $(RM) $(DIR_BONUS)/*.o
						$(RM) screenshot.bmp
						$(MAKE) fclean -C $(DIR_FT)
						$(MAKE) fclean -C $(DIR_PRINTF)
						$(MAKE) $@ -C $(DIR_MLX)

fclean				:	clean
						$(RM) $(NAME)

re					:	fclean all

.PHONY				:	all bonus clean fclean re commands start end dead
