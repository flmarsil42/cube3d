/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/10 14:12:54 by flmarsil          #+#    #+#             */
/*   Updated: 2020/03/11 12:42:21 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/cube3d.h"

int		launch_window(t_all *a)
{

	if (!(a->m.mlx_p = mlx_init()))
		return (EXIT_FAILURE);
	load_textures(a);
	if (a->p.flag == 0)
		a->m.window_p =
		mlx_new_window(a->m.mlx_p, a->p.rx, a->p.ry, "Cube3D");
	if (a->p.flag == 0 && !a->m.window_p)
		return (EXIT_FAILURE);
	if (!(a->i.image_p = mlx_new_image(a->m.mlx_p, a->p.rx, a->p.ry)))
		return (EXIT_FAILURE);
	if (!(a->i.data_addr = (int *)mlx_get_data_addr(a->i.image_p,
			&a->i.bits_pixel, &a->i.size_line, &a->i.endian)))
		return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}
