/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/22 09:44:26 by flmarsil          #+#    #+#             */
/*   Updated: 2020/02/05 09:13:44 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/get_next_line.h"

int		check_bsn(char *rst)
{
	int i;

	i = 0;
	if (!rst)
		return (0);
	while (rst[i] && rst[i] != '\n')
		i++;
	return ((rst[i] == '\n') ? 1 : 0);
}

size_t	ft_strlen2(char *s)
{
	int	i;

	i = 0;
	if (!s)
		return (0);
	while (s[i])
		i++;
	return (i);
}

void	ft_memset2(char *buf, size_t count)
{
	size_t i;

	i = -1;
	while (++i < count)
		((unsigned char *)buf)[i] = 0;
	return ;
}

char	*ft_strnew2(size_t size)
{
	char *ret;

	if (!(ret = malloc(sizeof(char) * size + 1)))
		return (NULL);
	ft_memset2(ret, size + 1);
	return (ret);
}

char	*ft_strjoin2(char **rst, char *buf)
{
	char	*ret;
	size_t	size;
	size_t	i;
	size_t	j;

	size = (ft_strlen2(*rst) + ft_strlen2(buf) + 1);
	if (!(ret = malloc(sizeof(char) * size)))
		return (NULL);
	i = -1;
	j = 0;
	while (*rst && (*rst)[j])
		ret[++i] = (*rst)[j++];
	while (buf && *buf)
		ret[++i] = *buf++;
	ret[++i] = '\0';
	if (*rst)
		free(*rst);
	return (ret);
}
