/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_textures.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/29 12:00:22 by flmarsil          #+#    #+#             */
/*   Updated: 2020/03/11 12:32:33 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/cube3d.h"

static void			bitmap_writer(int nb, int size, int fd)
{
	int				i;
	int				shift;

	i = 0;
	while (i < size)
	{
		shift = i * 8;
		ft_putchar_fd((nb & (0xff << shift)) >> (shift), fd);
		i++;
	}
}

static void			image_body(t_all *a, int fd)
{
	int				x;
	int				y;

	y = a->p.ry - 1;
	while (y >= 0)
	{
		x = 0;
		while (x < a->p.rx)
		{
			bitmap_writer(((int*)a->i.data_addr)[y * a->p.rx + x], 4, fd);
			x++;
		}
		y--;
	}
}

static void			bitmap_info_header(t_all *a, int fd)
{
	bitmap_writer(40, 4, fd);
	bitmap_writer(a->p.rx, 4, fd);
	bitmap_writer(a->p.ry, 4, fd);
	bitmap_writer(1, 2, fd);
	bitmap_writer(32, 2, fd);
	bitmap_writer(0, 4, fd);
	bitmap_writer(0, 4, fd);
	bitmap_writer(0, 4, fd);
	bitmap_writer(0, 4, fd);
	bitmap_writer(0, 4, fd);
	bitmap_writer(0, 4, fd);
}

static void			bitmap_file_header(t_all *a, int fd)
{
	unsigned int	size;

	size = a->p.rx * a->p.ry * 4 + 54;
	bitmap_writer(66, 1, fd);
	bitmap_writer(77, 1, fd);
	bitmap_writer(size, 4, fd);
	bitmap_writer(0, 2, fd);
	bitmap_writer(0, 2, fd);
	bitmap_writer(54, 4, fd);
}

void				ft_save_bitmap(const char *filename, t_all *a)
{
	int				fd;

	close(open(filename, O_RDONLY | O_CREAT, S_IRWXU));
	fd = open(filename, O_RDWR);
	bitmap_file_header(a, fd);
	bitmap_info_header(a, fd);
	image_body(a, fd);
	ft_close(a, 1);
}
