/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   textures.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/09 16:43:49 by flmarsil          #+#    #+#             */
/*   Updated: 2020/03/11 10:06:02 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/cube3d.h"

void	check_error_texture(char *line, int i)
{
	int		x;

	x = i;
	check_last_space(line);
	while (ft_isspace(line[x]) && line[x])
		x++;
	if (line[x] != '.' && line[x + 1] != '/')
		return (ft_error("Textures : Chemin non valide."));
	x++;
	while (line[x++])
	{
		(line[x] == '.' || line[x] == '/') ? x++ : 0;
		if (!ft_isalpha(line[x]) && line[x])
			return (ft_error("Textures : Chemin non valide."));
	}
}

char	*fill_texture(t_all *a, char *line, int i)
{
	char	*ret;
	int		size;
	int		j;

	i++;
	size = 0;
	j = 0;
	check_error_texture(line, i);
	while (ft_isspace(line[i]))
		i++;
	size = (line[i] == '.') ? ft_strlen(line) - i : 0;
	if (!(ret = malloc(sizeof(char) * size + 1)))
		return (NULL);
	while (line[i])
		ret[j++] = line[i++];
	ret[j] = '\0';
	a->p.isgood = 1;
	return (ret);
}
