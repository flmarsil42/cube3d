/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/09 16:44:37 by flmarsil          #+#    #+#             */
/*   Updated: 2020/03/11 14:06:55 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/cube3d.h"

void	last_check_map_bis(t_all *a, int y, int x)
{
	(y == 0 || y == a->p.my - 1)
		? ft_error("Map : erreur parsing.") : 0;
	(y > 0 && a->p.tabmap[y - 1][x] == 3)
		? ft_error("Map : erreur parsing.") : 0;
	(a->p.tabmap[y + 1][x] == 3)
		? ft_error("Map : erreur parsing.") : 0;
	(x > 0 && a->p.tabmap[y][x - 1] == 3)
		? ft_error("Map : erreur parsing..") : 0;
	(a->p.tabmap[y][x + 1] == 3)
		? ft_error("Map : erreur parsing.") : 0;
}

void	last_check_map(t_all *a)
{
	int y;
	int x;

	y = 0;
	while (y < a->p.my)
	{
		x = 0;
		while (x < a->p.mx - 1)
		{
			if (a->p.tabmap[y][x] == 2 || a->p.tabmap[y][x] == 0)
				last_check_map_bis(a, y, x);
			x++;
		}
		y++;
	}
	if (!a->p.player[0])
		return (ft_error("Map : 1 joueur obligatoire."));
}

void	map_conversion(t_all *a, char *line, int i)
{
	int		x;
	int		y;

	y = a->p.yconv;
	x = 0;
	i = -1;
	while (line[++i])
	{
		if (!ft_strchr(" 012NSEW", line[i]))
			return (ft_error("Map : Caractère non valide."));
		(line[i] == ' ') ? a->p.tabmap[y][x] = 3 : 0;
		(line[i] == '0') ? a->p.tabmap[y][x] = 0 : 0;
		(line[i] == '1') ? a->p.tabmap[y][x] = 1 : 0;
		(line[i] == '2') ? fill_sprite(a, y, x) : 0;
		(ft_strchr("NSEW", line[i])) ? fill_player(a, line, y, x) : 0;
		x++;
	}
	if (line[i] == '\0' && x < a->p.mx)
		while (x < a->p.mx)
			a->p.tabmap[y][x++] = 3;
	a->p.yconv++;
}

void	fill_numbers(t_all *a, char *line, int i)
{
	if (a->p.my < 3)
		return (ft_error("Map : Taille non valide"));
	map_conversion(a, line, i);
	a->p.isgood = 1;
	a->p.bloc = 1;
}
