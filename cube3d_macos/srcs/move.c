/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/18 15:58:20 by flmarsil          #+#    #+#             */
/*   Updated: 2020/03/11 14:17:25 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/cube3d.h"

void	move_ad(t_all *a)
{
	if (a->r.move_ad == 'D')
	{
		a->r.pos.x += a->r.plane.x * MOVSPEED;
		if (a->p.tabmap[(int)a->r.pos.x][(int)a->r.pos.y] > 0)
			a->r.pos.x -= a->r.plane.x * MOVSPEED;
		a->r.pos.y += a->r.plane.y * MOVSPEED;
		if (a->p.tabmap[(int)a->r.pos.x][(int)a->r.pos.y] > 0)
			a->r.pos.y -= a->r.plane.y * MOVSPEED;
	}
	else if (a->r.move_ad == 'A')
	{
		a->r.pos.x -= a->r.plane.x * MOVSPEED;
		if (a->p.tabmap[(int)a->r.pos.x][(int)a->r.pos.y] > 0)
			a->r.pos.x += a->r.plane.x * MOVSPEED;
		a->r.pos.y -= a->r.plane.y * MOVSPEED;
		if (a->p.tabmap[(int)a->r.pos.x][(int)a->r.pos.y] > 0)
			a->r.pos.y += a->r.plane.y * MOVSPEED;
	}
}

void	move(t_all *a)
{
	if (a->r.move == 'W')
	{
		a->r.pos.x += a->r.dir.x * MOVSPEED;
		if (a->p.tabmap[(int)a->r.pos.x][(int)a->r.pos.y] > 0)
			a->r.pos.x -= a->r.dir.x * MOVSPEED;
		a->r.pos.y += a->r.dir.y * MOVSPEED;
		if (a->p.tabmap[(int)a->r.pos.x][(int)a->r.pos.y] > 0)
			a->r.pos.y -= a->r.dir.y * MOVSPEED;
	}
	else if (a->r.move == 'S')
	{
		a->r.pos.x -= a->r.dir.x * MOVSPEED;
		if (a->p.tabmap[(int)a->r.pos.x][(int)a->r.pos.y] > 0)
			a->r.pos.x += a->r.dir.x * MOVSPEED;
		a->r.pos.y -= a->r.dir.y * MOVSPEED;
		if (a->p.tabmap[(int)a->r.pos.x][(int)a->r.pos.y] > 0)
			a->r.pos.y += a->r.dir.y * MOVSPEED;
	}
}

void	rotation(t_all *a)
{
	a->r.o_dir.x = a->r.dir.x;
	a->r.o_pla.x = a->r.plane.x;
	if (a->r.rot == 'Q')
	{
		a->r.dir.x = a->r.dir.x * cos(-ROTSPEED)
			- a->r.dir.y * sin(-ROTSPEED);
		a->r.dir.y = a->r.o_dir.x * sin(-ROTSPEED)
			+ a->r.dir.y * cos(-ROTSPEED);
		a->r.plane.x = a->r.plane.x * cos(-ROTSPEED)
			- a->r.plane.y * sin(-ROTSPEED);
		a->r.plane.y = a->r.o_pla.x * sin(-ROTSPEED)
			+ a->r.plane.y * cos(-ROTSPEED);
	}
	else if (a->r.rot == 'E')
	{
		a->r.dir.x = a->r.dir.x * cos(ROTSPEED)
			- a->r.dir.y * sin(ROTSPEED);
		a->r.dir.y = a->r.o_dir.x * sin(ROTSPEED)
			+ a->r.dir.y * cos(ROTSPEED);
		a->r.plane.x = a->r.plane.x * cos(ROTSPEED)
			- a->r.plane.y * sin(ROTSPEED);
		a->r.plane.y = a->r.o_pla.x * sin(ROTSPEED)
			+ a->r.plane.y * cos(ROTSPEED);
	}
}
