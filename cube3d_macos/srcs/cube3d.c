/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cube3d.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/05 08:34:54 by flmarsil          #+#    #+#             */
/*   Updated: 2020/03/11 12:01:07 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/cube3d.h"

int		main_loop(t_all *a)
{
	(a->r.rot) ? rotation(a) : 0;
	(a->r.move) ? move(a) : 0;
	(a->r.move_ad) ? move_ad(a) : 0;
	raycasting_loop(a);
	mlx_put_image_to_window(a->m.mlx_p, a->m.window_p, a->i.image_p, 0, 0);
	return (0);
}

int		function_hub(t_all *a)
{
	int ret;

	ret = 0;
	if ((ret = launch_window(a)) && ret == EXIT_FAILURE)
		return (-1);
	init_raycasting(a);
	if (a->p.flag == 's')
	{
		raycasting_loop(a);
		ft_save_bitmap("screenshot.bmp", a);
	}
	mlx_hook(a->m.window_p, 2, 0, &key_press, a);
	mlx_hook(a->m.window_p, 3, 0, &key_release, a);
	mlx_hook(a->m.window_p, 17, 0, &ft_close, a);
	mlx_loop_hook(a->m.mlx_p, &main_loop, a);
	mlx_loop(a->m.mlx_p);
	return (1);
}

void	ft_flags(t_all *a, char **av)
{
	if (!ft_strncmp(av[2], "--save", 7))
		a->p.flag = 's';
	else
		ft_error("Cube3d : Erreur argument.");
}

int		main(int ac, char **av)
{
	int				fd;
	int				ret;
	char			*map;
	t_all			a;

	ret = 0;
	if (ac < 2 || ac > 3)
		ft_error("Nombre d'arguments <= 3 - Map exigée.\n");
	if (!(map = ft_strrchr(av[1], '.')))
		ft_error("L'extension du fichier map doit etre .cub");
	if (ft_strncmp(".cub", map, 5))
		ft_error("L'extension du fichier map doit etre .cub");
	if ((fd = open(av[1], O_RDONLY)) == -1)
		ft_error("Erreur lecture");
	fill_size_tab(&a, fd);
	if ((fd = open(av[1], O_RDONLY)) == -1)
		ft_error("Erreur lecture");
	if ((ret = init_parsing(&a, fd)) && ret == -1)
		return (-1);
	(ac == 3) ? ft_flags(&a, av) : 0;
	if ((ret = function_hub(&a)) && ret == -1)
		ft_error("Erreur launch window");
	return (0);
}
