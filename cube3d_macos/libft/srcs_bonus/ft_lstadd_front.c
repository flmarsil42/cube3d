/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstadd_front.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user <user@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/19 12:55:45 by flmarsil          #+#    #+#             */
/*   Updated: 2019/11/28 13:30:51 by user             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

void	ft_lstadd_front(t_list **alst, t_list *new)
{
	if (!alst)
		new->next = 0;
	else
		new->next = *alst;
	*alst = new;
}
