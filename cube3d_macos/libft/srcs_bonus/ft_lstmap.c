/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: user <user@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/20 14:43:49 by flmarsil          #+#    #+#             */
/*   Updated: 2019/11/28 13:19:21 by user             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list *tmp;

	if (!lst)
		return (0);
	if (!(tmp = ft_lstnew((*f)(lst->content))))
	{
		ft_lstclear(&tmp, del);
		return (0);
	}
	tmp->next = ft_lstmap(lst->next, f, del);
	return (tmp);
}
