/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/12 13:54:00 by flmarsil          #+#    #+#             */
/*   Updated: 2020/01/24 13:51:45 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

char	*ft_strchr(const char *s, int c)
{
	while ((*s && *s != (char)c))
		s++;
	if (*s && *s == (char)c)
		return ((char *)s);
	return (NULL);
}
