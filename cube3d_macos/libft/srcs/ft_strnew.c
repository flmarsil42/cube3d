/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/22 14:55:39 by flmarsil          #+#    #+#             */
/*   Updated: 2020/01/24 13:51:31 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

char	*ft_strnew(size_t size)
{
	char *ret;

	if (!(ret = malloc(sizeof(char) * size + 1)))
		return (NULL);
	ft_memset(ret, 0, size + 1);
	ret[size] = '\0';
	return (ret);
}
