/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/12 16:59:05 by flmarsil          #+#    #+#             */
/*   Updated: 2019/11/21 10:54:41 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t		i;
	const char	*a;
	const char	*b;

	a = (char *)haystack;
	b = (char *)needle;
	if (!(*b))
		return ((char *)a);
	while (*a && len)
	{
		i = 0;
		while (a[i] == b[i] && a[i] && i < len)
			i++;
		if (b[i] == '\0')
			return ((char *)a);
		a++;
		len--;
	}
	return (NULL);
}
