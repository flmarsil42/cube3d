/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flmarsil <flmarsil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/12 10:15:23 by flmarsil          #+#    #+#             */
/*   Updated: 2019/12/03 14:48:30 by flmarsil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

void	*ft_memmove(void *dst, const void *src, size_t n)
{
	unsigned char *p_dst;
	unsigned char *p_src;

	if (!dst && !src && n)
		return (NULL);
	p_dst = (unsigned char*)dst;
	p_src = (unsigned char*)src;
	if (dst >= src)
		while (n--)
			p_dst[n] = p_src[n];
	else
		while (n--)
			*p_dst++ = *p_src++;
	return (dst);
}
