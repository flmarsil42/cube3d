# cub3d

Résumé: Explorer la technique du ray-casting. Faire une vue dynamique au sein d'un labyrinthe.


Skills:
- Imperative programming
- Rigor
- Graphics
- Algorithms & AI 

Lire le [sujet][1]

`Ce projet a été codé sur Macos et Linux`

### Compiler et lancer le projet

1. Téléchargez / Clonez le dépot

```
git clone https://gitlab.com/flmarsil42/cub3d && cd cub3d
```

2. Compilez le projet et utilisez le fichier ./Cube3d avec le fichier map.cub en argument avec lequel vous pouvez modifier les dimensions de la map.

```
Make
./Cube3d map.cub
```

[1]:https://gitlab.com/flmarsil42/cub3d/-/blob/master/en.cub3dsubject.pdf
