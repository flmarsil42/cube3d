#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>

# define BUFFER_SIZE 10000

int			get_next_line(int fd, char **line);
char		*ft_substr2(char **rst, unsigned int start);
void		ft_memset2(char *buf, size_t count);
char		*ft_strjoin2(char **s1, char *s2);
char		*ft_strnew2(size_t size);
char		*ft_strdup2(char *rst);
int			check_bsn(char *buf);
size_t		ft_strlen2(char *s);

#endif
