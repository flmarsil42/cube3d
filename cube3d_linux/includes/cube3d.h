#ifndef CUBE3D_H
# define CUBE3D_H
# include <unistd.h>
# include <stdlib.h>
# include <math.h>
# include <fcntl.h>
# include "keyboard.h"
# include "../minilibx/mlx.h"
# include "../libft/include/libft.h"
# include "../libftprintf/includes/libpf.h"
# include "../libftprintf/includes/ft_printf.h"
# include "get_next_line.h"

# define MOVSPEED 0.045
# define ROTSPEED 0.03
# define RENDERDIST 10

typedef struct		s_parsing
{
	char			flag;
	int				rx;
	int				ry;
	int				f;
	int				c;
	char			*texture[5];
	int				**tabmap;
	int				mx;
	int				my;
	int				yconv;
	int				player[3];
	int				bloc;
	char			type;
	int				isgood;
	int				len_security;
	int				num_c;
}					t_parsing;

typedef struct		s_vector
{
	int				x;
	int				y;
}					t_vector;

typedef struct		s_dvector
{
	double			x;
	double			y;
}					t_dvector;

typedef struct		s_img
{
	void			*image_p;
	int				*data_addr;
	int				bits_pixel;
	int				size_line;
	int				endian;
	int				width;
	int				height;
}					t_img;

typedef struct		s_display
{
	t_vector		loop;
	t_vector		lim;
}					t_display;

typedef struct		s_mlx
{
	void			*mlx_p;
	void			*window_p;
}					t_mlx;

typedef struct		s_sprite
{
	t_dvector		pos;
	int				type;
	double			dist;
}					t_sprite;

typedef struct		s_raycasting
{
	double			camera_x;
	t_dvector		pos;
	t_dvector		dir;
	t_dvector		plane;
	t_vector		map;
	t_dvector		o_pla;
	t_dvector		o_dir;
	t_dvector		raydir;
	t_dvector		delta_dist;
	t_dvector		side_dist;
	t_dvector		step;
	int				hit;
	int				side;
	int				line_height;
	int				draw_start;
	int				draw_end;
	char			move;
	char			move_ad;
	char			rot;
	t_img			text[5];
	int				tex_nb;
	double			wall_dist;
	double			wall_pos;
	int				tex_x;
	int				tex_y;
	double			text_step;
	double			text_pos;
	t_sprite		*sprites;
	t_dvector		sprite;
	t_vector		draw_sprite_start;
	t_vector		draw_sprite_end;
	t_dvector		transform;
	double			*zbuffer;
	int				stripe;
	int				nb_sprites;
	int				sprite_height;
	int				sprite_width;
	double			sprite_screen_x;
	double			inv_det;
}					t_raycasting;

typedef struct		s_all
{
	t_parsing		p;
	t_img			i;
	t_raycasting	r;
	t_mlx			m;
	t_sprite		s;
	t_display		d;
}					t_all;

void				check_error_fc(t_all *a, char *line, int i, int *tab);
void				check_error_r(char *line, int i, t_all *a);
void				check_error_texture(char *line, int i);
void				check_last_space(char *line);
int					check_structure(t_all *a);
void				last_check_map(t_all *a);

void				ft_error(char *error);
int					numbers_count(char *line, int i, t_all *a);
int					comma_count(char *line, int x);
int					ft_iscomma(char c);
void				init_struct(t_all *a);
int					len_security(t_all *a, char *line, int x);

int					init_parsing(t_all *a, int fd);
void				map_parsing(t_all *a, char *line);
void				map_parsing_bis(t_all *a, char *line, int i);
char				*fill_texture(t_all *a, char *line, int i);
void				fill_numbers(t_all *a, char *line, int i);
int					fill_sprite(t_all *a, int y, int x);
void				fc_colors(t_all *a, char *line, int i);
void				screen_resolution(t_all *a, char *line, int i);
void				map_conversion(t_all *a, char *line, int i);
int					fill_size_tab(t_all *a, int fd);
int					tabmap_malloc(t_all *a);
void				fill_player(t_all *a, char *line, int y, int x);

int					launch_window(t_all *a);
void				load_textures(t_all *a);
void				del_exit(t_all *all);
void				ft_exit(t_all *a);
int					ft_close(t_all *a, int ret_exit);
void				ft_draw(t_all *a, int x);
void				ft_save_bitmap(const char *filename, t_all *a);

void				raycasting_loop(t_all *a);
int					init_raycasting(t_all *a);

void				draw_sprite(t_all *a, int i);
void				raycast_sprites(t_all *a);
void				sort_sprites(t_all *a);
void				swap_sprites(t_all *a, int i);

int					key_release(int keycode, t_all *a);
int					key_press(int keycode, t_all *a);

void				move_ad(t_all *a);
void				move(t_all *a);
void				rotation(t_all *a);

#endif
