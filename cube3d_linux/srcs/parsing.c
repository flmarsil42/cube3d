#include "../includes/cube3d.h"

void	check_last_space(char *line)
{
	int		len;

	len = ft_strlen(line);
	if (ft_isspace(line[len - 1]))
		return (ft_error("Parsing : Aucun espace autorisé en fin de ligne."));
}

int		check_structure(t_all *a)
{
	int		i;

	i = -1;
	while (++i < 5)
		if ((a->p.texture)[i] == NULL)
			return (0);
	if (!a->p.rx || !a->p.ry || a->p.c == -1 || a->p.f == -1)
		return (0);
	return (1);
}

void	map_parsing_bis(t_all *a, char *line, int i)
{
	if (line[0] == 'E' && line[i + 1] == 'A'
		&& line[i + 2] == ' ' && ++i && !a->p.texture[3])
		a->p.texture[3] = fill_texture(a, line, i);
	else if (line[0] == 'S' && line[i + 1] == ' ' && ++i && !a->p.texture[4])
		a->p.texture[4] = fill_texture(a, line, i);
	else if (check_structure(a) && (line[0] == '1' || line[0] == ' ')
		&& line[ft_strlen(line) - 1] == '1')
		fill_numbers(a, line, i);
	else if (line[0] == '\0' && a->p.bloc == 0)
		a->p.isgood = 1;
	else
		return (ft_error("Parsing : Problème parsing"));
}

void	map_parsing(t_all *a, char *line)
{
	int		i;

	i = 0;
	a->p.type = line[0];
	if (!ft_strchr(" RFCNSWE1\0", line[0]) && line[0])
		return (ft_error("Parsing : Caractère non valide"));
	else if (line[0] == 'R' && line[i + 1] == ' ' && ++i
		&& (!a->p.rx && !a->p.ry))
		screen_resolution(a, line, i);
	else if ((line[0] == 'F') && line[i + 1] == ' ' && ++i && a->p.f == -1)
		fc_colors(a, line, i);
	else if ((line[0] == 'C') && line[i + 1] == ' ' && ++i && a->p.c == -1)
		fc_colors(a, line, i);
	else if (line[0] == 'N' && line[i + 1] == 'O'
		&& line[i + 2] == ' ' && ++i && !a->p.texture[0])
		a->p.texture[0] = fill_texture(a, line, i);
	else if (line[0] == 'S' && line[i + 1] == 'O'
		&& line[i + 2] == ' ' && ++i && !a->p.texture[1])
		a->p.texture[1] = fill_texture(a, line, i);
	else if (line[0] == 'W' && line[i + 1] == 'E'
		&& line[i + 2] == ' ' && ++i && !a->p.texture[2])
		a->p.texture[2] = fill_texture(a, line, i);
	else
		map_parsing_bis(a, line, i);
}

int		init_parsing(t_all *a, int fd)
{
	char		*line;
	int			ret;
	int			sec;

	line = NULL;
	ret = 1;
	sec = 0;
	init_struct(a);
	if ((sec = tabmap_malloc(a)) && sec == -1)
	{
		ft_error("Malloc tabmap");
		ft_close(a, -1);
	}
	while (ret > 0)
	{
		ret = get_next_line(fd, &line);
		map_parsing(a, line);
		a->p.isgood = 0;
		free(line);
		(ret == 0) ? last_check_map(a) : 0;
	}
	if (close(fd) == -1)
		return (-1);
	return (0);
}
