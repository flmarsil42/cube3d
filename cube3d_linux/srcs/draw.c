#include "../includes/cube3d.h"

void	draw_ceiling(t_all *a, int x)
{
	int			i;
	int			color;

	i = 0;
	color = a->p.c;
	while (i++ <= a->r.draw_start)
		a->i.data_addr[i * a->p.rx + x] = color;
}

void	draw_walls(t_all *a, int x)
{
	int			i;
	int			color;

	i = a->r.draw_start;
	while (i++ < a->r.draw_end)
	{
		a->r.tex_y = (int)a->r.text_pos & (a->r.text[a->r.tex_nb].height - 1);
		a->r.text_pos += a->r.text_step;
		color = a->r.text[a->r.tex_nb].data_addr
			[a->r.tex_y * a->r.text[a->r.tex_nb].width + a->r.tex_x];
		a->i.data_addr[i * a->p.rx + x] = color;
		a->r.zbuffer[x] = a->r.wall_dist;
	}
}

void	draw_floor(t_all *a, int x)
{
	int			i;
	int			color;

	color = a->p.f;
	i = a->p.ry;
	while (i-- >= a->r.draw_end + 2 && i > 0)
		a->i.data_addr[i * a->p.rx + x] = color;
}

void	ft_draw(t_all *a, int x)
{
	draw_ceiling(a, x);
	draw_walls(a, x);
	draw_floor(a, x);
}
