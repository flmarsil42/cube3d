#include "../includes/cube3d.h"

int		launch_window(t_all *a)
{
	if (!(a->m.mlx_p = mlx_init()))
		return (EXIT_FAILURE);
	load_textures(a);
	mlx_get_screen_size(a->m.mlx_p, &a->d.lim.x, &a->d.lim.y);
	if (a->p.rx > a->d.lim.x)
		a->p.rx = a->d.lim.x;
	if (a->p.ry > a->d.lim.y)
		a->p.ry = a->d.lim.y;
	if (a->p.flag == 0)
		a->m.window_p =
		mlx_new_window(a->m.mlx_p, a->p.rx, a->p.ry, "Cube3D");
	if (a->p.flag == 0 && !a->m.window_p)
		return (EXIT_FAILURE);
	if (!(a->i.image_p = mlx_new_image(a->m.mlx_p, a->p.rx, a->p.ry)))
		return (EXIT_FAILURE);
	if (!(a->i.data_addr = (int *)mlx_get_data_addr(a->i.image_p,
			&a->i.bits_pixel, &a->i.size_line, &a->i.endian)))
		return (EXIT_FAILURE);
	return (EXIT_SUCCESS);
}
