#include "../includes/cube3d.h"

int		load_ns_textures(t_all *a)
{
	if (!(a->r.text[0].image_p =
		mlx_xpm_file_to_image(a->m.mlx_p, a->p.texture[0],
			&a->r.text[0].width, &a->r.text[0].height)))
		return (EXIT_FAILURE);
	if (!(a->r.text[0].data_addr =
		(int *)mlx_get_data_addr(a->r.text[0].image_p, &a->r.text[0].bits_pixel,
			&a->r.text[0].size_line, &a->r.text[0].endian)))
		return (EXIT_FAILURE);
	if (!(a->r.text[1].image_p =
		mlx_xpm_file_to_image(a->m.mlx_p, a->p.texture[1],
			&a->r.text[1].width, &a->r.text[1].height)))
		return (EXIT_FAILURE);
	if (!(a->r.text[1].data_addr =
		(int *)mlx_get_data_addr(a->r.text[1].image_p, &a->r.text[1].bits_pixel,
			&a->r.text[1].size_line, &a->r.text[1].endian)))
		return (EXIT_FAILURE);
	return (0);
}

int		load_we_textures(t_all *a)
{
	if (!(a->r.text[2].image_p =
		mlx_xpm_file_to_image(a->m.mlx_p, a->p.texture[3], &a->r.text[2].width,
			&a->r.text[2].height)))
		return (EXIT_FAILURE);
	if (!(a->r.text[2].data_addr =
		(int *)mlx_get_data_addr(a->r.text[2].image_p, &a->r.text[2].bits_pixel,
			&a->r.text[2].size_line, &a->r.text[2].endian)))
		return (EXIT_FAILURE);
	if (!(a->r.text[3].image_p =
		mlx_xpm_file_to_image(a->m.mlx_p, a->p.texture[2], &a->r.text[3].width,
			&a->r.text[3].height)))
		return (EXIT_FAILURE);
	if (!(a->r.text[3].data_addr =
		(int *)mlx_get_data_addr(a->r.text[3].image_p, &a->r.text[3].bits_pixel,
			&a->r.text[3].size_line, &a->r.text[3].endian)))
		return (EXIT_FAILURE);
	return (0);
}

int		load_sprites_textures(t_all *a)
{
	if (!(a->r.text[4].image_p =
		mlx_xpm_file_to_image(a->m.mlx_p, a->p.texture[4], &a->r.text[4].width,
			&a->r.text[4].height)))
		return (EXIT_FAILURE);
	if (!(a->r.text[4].data_addr =
		(int *)mlx_get_data_addr(a->r.text[4].image_p, &a->r.text[4].bits_pixel,
			&a->r.text[4].size_line, &a->r.text[4].endian)))
		return (EXIT_FAILURE);
	return (0);
}

void	load_textures(t_all *a)
{
	if (load_ns_textures(a))
		ft_error("Textures : Le chargement a écouché.");
	if (load_we_textures(a))
		ft_error("Textures : Le chargement a écouché.");
	if (load_sprites_textures(a))
		ft_error("Textures : Le chargement a écouché.");
}
