#include "../includes/cube3d.h"

int		tabmap_malloc(t_all *a)
{
	int y;

	y = 0;
	if (!(a->p.tabmap = calloc(sizeof(int *), a->p.my)))
		return (-1);
	while (y < a->p.my)
		if (!(a->p.tabmap[y++] = calloc(sizeof(int), a->p.mx)))
			return (-1);
	return (0);
}

void	check_last_line(t_all *a, int i, char *buf)
{
	int		len;

	len = -1;
	if (!buf[0])
		return (ft_error("Fichier vide"));
	while (buf[--i] != '\n')
		++len;
	(a->p.mx < len) ? a->p.mx = len : 0;
}

void	fill_size_tab_bis(t_all *a, char *buf, int i)
{
	int		actual_max;

	actual_max = 0;
	while (buf[++i])
	{
		if (ft_strchr(" 012", buf[i]) && (ft_strchr(" 012", buf[i + 1])
			|| buf[i + 1] == '\n') && buf[i - 1] == '\n')
		{
			while (buf[i])
			{
				(buf[i] == '2') ? a->r.nb_sprites++ : 0;
				if (buf[i] == '\n')
				{
					(ft_strchr(" 012", buf[i - 1])
						&& ft_strchr(" 012", buf[i + 1])) ? a->p.my++ : 0;
					(actual_max > a->p.mx) ? a->p.mx = actual_max : 0;
					actual_max = 0;
				}
				else
					actual_max++;
				i++;
			}
		}
	}
	check_last_line(a, i, buf);
}

int		fill_size_tab(t_all *a, int fd)
{
	int		i;
	char	buf[BUFFER_SIZE + 1];

	i = -1;
	a->p.my = 1;
	a->p.mx = 0;
	a->p.yconv = 0;
	a->r.nb_sprites = 0;
	ft_memset(buf, 0, BUFFER_SIZE + 1);
	if (read(fd, buf, BUFFER_SIZE) == -1)
		return (-1);
	fill_size_tab_bis(a, buf, i);
	if (close(fd) == -1)
		return (-1);
	return (0);
}
