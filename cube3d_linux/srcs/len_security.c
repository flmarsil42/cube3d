#include "../includes/cube3d.h"

int		len_security(t_all *a, char *line, int x)
{
	if ((a->p.len_security > 3 && (a->p.type == 'F' || a->p.type == 'C'))
		|| (a->p.len_security > 4 && (a->p.type == 'R')))
		return (-1);
	while (line[x] && ft_isdigit(line[x]))
		x++;
	if (line[x] != ','
		&& a->p.num_c != 3 && (a->p.type == 'C' || a->p.type == 'F'))
		return (-1);
	return (0);
}
