#include "../includes/cube3d.h"

void	draw_sprite_on_display(t_all *a, int i)
{
	int			color;
	size_t		d;
	int			y;

	y = a->r.draw_sprite_start.y;
	while (++y < a->r.draw_sprite_end.y)
	{
		d = y * 256 - a->p.ry * 128 + a->r.sprite_height * 128;
		a->r.tex_y = ((d * a->r.text[4 + a->r.sprites[i].type].height) /
		a->r.sprite_height) / 256;
		color =
			a->r.text[4 + a->r.sprites[i].type].data_addr[a->r.text[4 +
				a->r.sprites[i].type].width * a->r.tex_y + a->r.tex_x];
		if ((color & 0xffffff) != 0)
			a->i.data_addr[y * a->p.rx + a->r.stripe] = color;
	}
}

void	draw_sprite(t_all *a, int i)
{
	a->r.stripe = a->r.draw_sprite_start.x;
	while (a->r.stripe < a->r.draw_sprite_end.x)
	{
		a->r.tex_x = (int)((256 * (a->r.stripe -
		(-a->r.sprite_width / 2 + a->r.sprite_screen_x)) *
		a->r.text[4 + a->r.sprites[i].type].width / a->r.sprite_width) / 256);
		if (a->r.transform.y > 0 && a->r.stripe > 0 && a->r.stripe < a->p.rx &&
			a->r.transform.y < a->r.zbuffer[a->r.stripe])
			draw_sprite_on_display(a, i);
		a->r.stripe++;
	}
}
