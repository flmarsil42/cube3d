#include "../includes/cube3d.h"

void	check_error_r(char *line, int i, t_all *a)
{
	int		x;
	int		numbers;

	x = i;
	numbers = 0;
	while (line[x++])
		if ((!ft_strchr("0123456789", line[x])
			&& !ft_isspace(line[x]) && line[x]))
			return (ft_error("Résolution : Caractère non valide."));
	x = i;
	if ((numbers = numbers_count(line, x, a)) && numbers != 2)
		return (ft_error("Résolution : non valide."));
	check_last_space(line);
}

void	screen_resolution(t_all *a, char *line, int i)
{
	check_error_r(line, i, a);
	while (ft_isspace(line[i]))
		i++;
	while (ft_isdigit(line[i]))
		a->p.rx = a->p.rx * 10 + (line[i++] - '0');
	i++;
	while (ft_isspace(line[i]))
		i++;
	while (ft_isdigit(line[i]))
		a->p.ry = a->p.ry * 10 + (line[i++] - '0');
	a->p.isgood = 1;
}
