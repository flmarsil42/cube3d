#include "../includes/cube3d.h"

void	check_error_fc(t_all *a, char *line, int i, int *tab)
{
	int		x;
	int		numbers;
	int		comma;

	x = i;
	numbers = 0;
	comma = 0;
	if (a->p.isgood == 0)
	{
		while (line[x++])
			if ((!ft_strchr("0123456789,", line[x])
				&& !ft_isspace(line[x]) && line[x]))
				return (ft_error("F/C : Caractère non valide."));
		x = i;
		if ((numbers = numbers_count(line, x, a)) && numbers != 3)
			return (ft_error("F/C : Couleurs non valides."));
		x = i;
		if ((comma = comma_count(line, x)) && comma != 2)
			return (ft_error("F/C : Couleurs non valides."));
	}
	if (tab[0] > 255 || tab[1] > 255 || tab[2] > 255)
		return (ft_error("F/C : Couleurs non valides."));
	check_last_space(line);
}

void	fc_colors(t_all *a, char *line, int i)
{
	int		x;
	int		tab[3];
	int		conversion;

	x = 0;
	tab[0] = 0;
	tab[1] = 0;
	tab[2] = 0;
	conversion = 0;
	check_error_fc(a, line, i, tab);
	while (line[i++])
	{
		while ((ft_isspace(line[i]) || ft_iscomma(line[i])))
			i++;
		while (ft_isdigit(line[i]))
			tab[x] = tab[x] * 10 + (line[i++] - '0');
		(!ft_isdigit(line[i])) ? x++ : 0;
	}
	((a->p.isgood = 2) && a->p.isgood == 2)
		? check_error_fc(a, line, i, tab) : 0;
	conversion = 256 * 256 * tab[0] + 256 * tab[1] + tab[2];
	(a->p.type == 'F') ? (a->p.f = conversion) : 0;
	(a->p.type == 'C') ? (a->p.c = conversion) : 0;
	a->p.isgood = 1;
}
