#include "../includes/cube3d.h"

void	raycast_set_draw(t_all *a)
{
	a->r.sprite_height = (int)fabs((a->p.ry / (a->r.transform.y)));
	a->r.draw_sprite_start.y = (-a->r.sprite_height / 2 + a->p.ry / 2);
	if (a->r.draw_sprite_start.y < 0)
		a->r.draw_sprite_start.y = 0;
	a->r.draw_sprite_end.y = (a->r.sprite_height / 2 + a->p.ry / 2);
	if (a->r.draw_sprite_end.y >= a->p.ry)
		a->r.draw_sprite_end.y = a->p.ry - 1;
	a->r.sprite_width = (int)fabs((a->p.ry / (a->r.transform.y)));
	a->r.draw_sprite_start.x = -a->r.sprite_width / 2 + a->r.sprite_screen_x;
	if (a->r.draw_sprite_start.x < 0)
		a->r.draw_sprite_start.x = 0;
	a->r.draw_sprite_end.x = a->r.sprite_width / 2 + a->r.sprite_screen_x;
	if (a->r.draw_sprite_end.x >= a->p.rx)
		a->r.draw_sprite_end.x = a->p.rx - 1;
}

void	raycast_sprites(t_all *a)
{
	int	i;

	i = 0;
	sort_sprites(a);
	while ((int)i < a->r.nb_sprites)
	{
		a->r.sprite.x = a->r.sprites[i].pos.x - a->r.pos.x;
		a->r.sprite.y = a->r.sprites[i].pos.y - a->r.pos.y;
		a->r.inv_det = 1.0 / ((a->r.plane.x * a->r.dir.y)
			- (a->r.dir.x * a->r.plane.y));
		a->r.transform.x = a->r.inv_det * ((a->r.dir.y * a->r.sprite.x)
			- (a->r.dir.x * a->r.sprite.y));
		a->r.transform.y = a->r.inv_det * ((-a->r.plane.y * a->r.sprite.x)
			+ (a->r.plane.x * a->r.sprite.y));
		a->r.sprite_screen_x = (int)((a->p.rx / 2) *
			(1 + a->r.transform.x / a->r.transform.y));
		raycast_set_draw(a);
		draw_sprite(a, i);
		i++;
	}
}
