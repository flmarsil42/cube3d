#include "../includes/cube3d.h"

void	search_walls(t_all *a)
{
	a->r.map.x = (int)a->r.pos.x;
	a->r.map.y = (int)a->r.pos.y;
	a->r.delta_dist.x = fabs(1 / a->r.raydir.x);
	a->r.delta_dist.y = fabs(1 / a->r.raydir.y);
	a->r.hit = 0;
	if (a->r.raydir.x < 0)
	{
		a->r.step.x = -1;
		a->r.side_dist.x = (a->r.pos.x - a->r.map.x) * a->r.delta_dist.x;
	}
	else
	{
		a->r.step.x = 1;
		a->r.side_dist.x = (a->r.map.x + 1.0 - a->r.pos.x) * a->r.delta_dist.x;
	}
	if (a->r.raydir.y < 0)
	{
		a->r.step.y = -1;
		a->r.side_dist.y = (a->r.pos.y - a->r.map.y) * a->r.delta_dist.y;
	}
	else
	{
		a->r.step.y = 1;
		a->r.side_dist.y = (a->r.map.y + 1.0 - a->r.pos.y) * a->r.delta_dist.y;
	}
}

void	perform_dda(t_all *a)
{
	while (a->r.hit == 0)
	{
		if (a->r.side_dist.x < a->r.side_dist.y)
		{
			a->r.side_dist.x += a->r.delta_dist.x;
			a->r.map.x += a->r.step.x;
			a->r.side = 0;
		}
		else
		{
			a->r.side_dist.y += a->r.delta_dist.y;
			a->r.map.y += a->r.step.y;
			a->r.side = 1;
		}
		if (a->p.tabmap[a->r.map.x][a->r.map.y] == 1)
			a->r.hit = 1;
	}
}

void	setup_draw(t_all *a)
{
	if (a->r.side == 0)
		a->r.wall_dist =
			(a->r.map.x - a->r.pos.x + (1 - a->r.step.x) / 2) / a->r.raydir.x;
	else
		a->r.wall_dist =
			(a->r.map.y - a->r.pos.y + (1 - a->r.step.y) / 2) / a->r.raydir.y;
	a->r.line_height = (int)(a->p.ry / a->r.wall_dist);
	a->r.draw_start = -a->r.line_height / 2 + a->p.ry / 2;
	if (a->r.draw_start < 0)
		a->r.draw_start = 0;
	a->r.draw_end = a->r.line_height / 2 + a->p.ry / 2;
	if (a->r.draw_end >= a->p.ry)
		a->r.draw_end = a->p.ry - 1;
}

void	raycast_textures(t_all *a)
{
	if (a->r.side)
		a->r.tex_nb = a->r.raydir.y < 0 ? 3 : 2;
	else
		a->r.tex_nb = a->r.raydir.x < 0 ? 0 : 1;
	if (a->r.side == 0)
		a->r.wall_pos = a->r.pos.y + a->r.wall_dist * a->r.raydir.y;
	else
		a->r.wall_pos = a->r.pos.x + a->r.wall_dist * a->r.raydir.x;
	a->r.wall_pos -= (int)a->r.wall_pos;
	a->r.tex_x = (int)(a->r.wall_pos * (double)a->r.text[a->r.tex_nb].width);
	if (a->r.side == 0 && a->r.raydir.x > 0)
		a->r.tex_x = a->r.text[a->r.tex_nb].width - a->r.tex_x - 1;
	if (a->r.side == 1 && a->r.raydir.y < 0)
		a->r.tex_x = a->r.text[a->r.tex_nb].width - a->r.tex_x - 1;
	a->r.text_step = 1.0 * a->r.text[a->r.tex_nb].height / a->r.line_height;
	a->r.text_pos =
		(a->r.draw_start - a->p.ry / 2 + a->r.line_height / 2) * a->r.text_step;
}

void	raycasting_loop(t_all *a)
{
	int x;

	x = 0;
	while (x < a->p.rx)
	{
		a->r.camera_x = 2 * x / (double)a->p.rx - 1;
		a->r.raydir.x = a->r.dir.x + a->r.plane.x * a->r.camera_x;
		a->r.raydir.y = a->r.dir.y + a->r.plane.y * a->r.camera_x;
		search_walls(a);
		perform_dda(a);
		setup_draw(a);
		raycast_textures(a);
		ft_draw(a, x);
		x++;
	}
	raycast_sprites(a);
}
