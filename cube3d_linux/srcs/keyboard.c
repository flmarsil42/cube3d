#include "../includes/cube3d.h"

int		key_press(int keycode, t_all *a)
{
	(keycode == KEY_W) ? a->r.move = 'W' : 0;
	(keycode == KEY_S) ? a->r.move = 'S' : 0;
	(keycode == KEY_A) ? a->r.move_ad = 'A' : 0;
	(keycode == KEY_D) ? a->r.move_ad = 'D' : 0;
	(keycode == ARROW_LEFT) ? a->r.rot = 'E' : 0;
	(keycode == ARROW_RIGHT) ? a->r.rot = 'Q' : 0;
	return (1);
}

int		key_release(int keycode, t_all *a)
{
	(keycode == KEY_ESC) ? ft_close(a, 0) : 0;
	(keycode == KEY_W) ? a->r.move = 0 : 0;
	(keycode == KEY_S) ? a->r.move = 0 : 0;
	(keycode == KEY_A) ? a->r.move_ad = 0 : 0;
	(keycode == KEY_D) ? a->r.move_ad = 0 : 0;
	(keycode == ARROW_LEFT) ? a->r.rot = 0 : 0;
	(keycode == ARROW_RIGHT) ? a->r.rot = 0 : 0;
	return (1);
}
