#include "../includes/cube3d.h"

void	init_direction(t_all *a)
{
	if (a->p.player[0] == 'N')
	{
		a->r.dir.x = -1;
		a->r.dir.y = 0;
	}
	else if (a->p.player[0] == 'S')
	{
		a->r.dir.x = 1;
		a->r.dir.y = 0;
	}
	else if (a->p.player[0] == 'E')
	{
		a->r.dir.x = 0;
		a->r.dir.y = 1;
	}
	else if (a->p.player[0] == 'W')
	{
		a->r.dir.x = 0;
		a->r.dir.y = -1;
	}
}

void	init_plane(t_all *a)
{
	if (a->p.player[0] == 'N')
	{
		a->r.plane.x = 0;
		a->r.plane.y = 0.66;
	}
	else if (a->p.player[0] == 'S')
	{
		a->r.plane.x = 0;
		a->r.plane.y = -0.66;
	}
	else if (a->p.player[0] == 'E')
	{
		a->r.plane.x = 0.66;
		a->r.plane.y = 0;
	}
	else if (a->p.player[0] == 'W')
	{
		a->r.plane.x = -0.66;
		a->r.plane.y = 0;
	}
}

int		init_raycasting(t_all *a)
{
	if (!(a->r.zbuffer = malloc(sizeof(double) * a->p.rx)))
		return (-1);
	a->r.pos.x = (double)a->p.player[1] + (double)0.5;
	a->r.pos.y = (double)a->p.player[2] + (double)0.5;
	init_direction(a);
	init_plane(a);
	return (0);
}
