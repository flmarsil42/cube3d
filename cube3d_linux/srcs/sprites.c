#include "../includes/cube3d.h"

int		fill_sprite(t_all *a, int y, int x)
{
	a->p.tabmap[y][x] = 2;
	if (!a->r.sprites)
	{
		if (!(a->r.sprites = malloc(sizeof(t_sprite) * a->r.nb_sprites)))
			return (EXIT_FAILURE);
		a->r.nb_sprites = 0;
	}
	a->r.sprites[a->r.nb_sprites].pos.y = x + 0.5;
	a->r.sprites[a->r.nb_sprites].pos.x = y + 0.5;
	a->r.sprites[a->r.nb_sprites].type = 0;
	a->r.nb_sprites++;
	return (0);
}

void	swap_sprites(t_all *a, int i)
{
	t_sprite	tmp;

	tmp = a->r.sprites[i + 1];
	a->r.sprites[i + 1] = a->r.sprites[i];
	a->r.sprites[i] = tmp;
}

void	sort_sprites(t_all *a)
{
	int i;

	i = 0;
	while (i < a->r.nb_sprites)
	{
		a->r.sprites[i].dist = sqrt((a->r.pos.x - a->r.sprites[i].pos.x) *
			(a->r.pos.x - a->r.sprites[i].pos.x) +
			((a->r.pos.y - a->r.sprites[i].pos.y) *
			(a->r.pos.y - a->r.sprites[i].pos.y)));
		i++;
	}
	i = 0;
	while (i < a->r.nb_sprites - 1)
	{
		if (a->r.sprites[i].dist < a->r.sprites[i + 1].dist)
		{
			swap_sprites(a, i);
			i = 0;
		}
		i++;
	}
	i = 0;
}
