#include "../includes/get_next_line.h"

char	*ft_strdup2(char *rst)
{
	int		i;
	int		size;
	char	*dst;

	i = 0;
	size = ft_strlen2(rst) + 1;
	if (!(dst = malloc(sizeof(char) * size)))
		return (NULL);
	while (rst[i] != '\n')
	{
		if (rst[i] == '\0')
			break ;
		dst[i] = rst[i];
		i++;
	}
	dst[i] = '\0';
	return (dst);
}

char	*ft_substr2(char **rst, unsigned int start)
{
	char	*ret;
	size_t	size;
	size_t	i;
	size_t	j;

	size = (ft_strlen2(*rst) - start + 1);
	if (start > ft_strlen2(*rst))
	{
		free(*rst);
		return (NULL);
	}
	if (!rst || !(ret = malloc(sizeof(char) * size)))
		return (NULL);
	i = start;
	j = 0;
	while ((*rst)[i])
		ret[j++] = (*rst)[i++];
	ret[j] = '\0';
	if (*rst)
		free(*rst);
	return (ret);
}

int		get_next_line(int fd, char **line)
{
	char		buf[BUFFER_SIZE + 1];
	int			nread;
	static char	*rst;

	if (read(fd, buf, 0) == -1 || !line || BUFFER_SIZE < 1)
		return (-1);
	if (!rst && !(*line))
		rst = ft_strnew2(1);
	nread = 1;
	ft_memset2(buf, BUFFER_SIZE + 1);
	while (!check_bsn(rst) && (nread = read(fd, buf, BUFFER_SIZE)) > 0)
	{
		if (!(rst = ft_strjoin2(&rst, buf)))
			return (-1);
		ft_memset2(buf, BUFFER_SIZE + 1);
	}
	if (rst)
	{
		if (!(*line = ft_strdup2(rst)))
			return (-1);
		rst = ft_substr2(&rst, (ft_strlen2(*line) + 1));
	}
	if (!nread)
		return (0);
	return (1);
}
