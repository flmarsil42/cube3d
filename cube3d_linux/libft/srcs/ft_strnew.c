#include "../include/libft.h"

char	*ft_strnew(size_t size)
{
	char *ret;

	if (!(ret = malloc(sizeof(char) * size + 1)))
		return (NULL);
	ft_memset(ret, 0, size + 1);
	ret[size] = '\0';
	return (ret);
}
