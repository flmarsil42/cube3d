#include "../include/libft.h"

int			ft_word_count(char const *s, char c)
{
	int count;

	count = 0;
	while (*s)
	{
		while (*s == (char)c)
			s++;
		if (*s && (*s != (char)c))
			++count;
		while (*s && (*s != (char)c))
			s++;
	}
	return (count);
}

char		*ft_malloc_word(char const *s, char c)
{
	char	*word;
	int		size;
	int		i;

	size = 0;
	while (s[size] && s[size] != c)
		size++;
	if (!(word = malloc(sizeof(char) * (size + 1))))
		return (NULL);
	i = 0;
	while (s[i] && s[i] != c)
	{
		word[i] = s[i];
		i++;
	}
	word[i] = '\0';
	return (word);
}

static char	**ft_free(int words, char **ret)
{
	int i;

	i = 0;
	while (i < words)
		free(ret[i++]);
	free(ret);
	ret = 0;
	return (0);
}

char		**ft_split(char const *s, char c)
{
	char	**ret;
	int		words;
	int		i;

	if (!s)
		return (NULL);
	words = ft_word_count(s, c) + 1;
	if (!(ret = malloc(sizeof(char *) * words)))
		return (NULL);
	i = 0;
	while (*s)
	{
		while (*s && (*s == (char)c))
			s++;
		if (*s && (*s != (char)c))
			if (!(ret[i++] = ft_malloc_word(s, c)))
				return (ft_free(words, ret));
		while (*s && (*s != (char)c))
			s++;
	}
	ret[i] = NULL;
	return (ret);
}
