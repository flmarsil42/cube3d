#include "../include/libft.h"

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t		i;
	const char	*a;
	const char	*b;

	a = (char *)haystack;
	b = (char *)needle;
	if (!(*b))
		return ((char *)a);
	while (*a && len)
	{
		i = 0;
		while (a[i] == b[i] && a[i] && i < len)
			i++;
		if (b[i] == '\0')
			return ((char *)a);
		a++;
		len--;
	}
	return (NULL);
}
