#include "../include/libft.h"

char	*ft_strchr(const char *s, int c)
{
	while ((*s && *s != (char)c))
		s++;
	if (*s && *s == (char)c)
		return ((char *)s);
	return (NULL);
}
