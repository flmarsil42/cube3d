#ifndef LIBPF_H
# define LIBPF_H

# include <stdlib.h>
# include <string.h>
# include <unistd.h>

int					pf_atoi(const char *str);
int					pf_isdigit(int c);
size_t				pf_strlen(const char *str);

void				*pf_calloc(size_t count, size_t size);
void				*pf_memset(void *b, int c, size_t len);
void				pf_putchar_fd(char c, int fd);
void				pf_putstr_fd(char *s, int fd);

char				*pf_itoa_base(unsigned int value, char *base_to);
char				*pf_itoa_addr(long value, char *base);
char				*pf_itoa(int n);
char				*pf_strchr(const char *s, int c);

char				*pf_strdup(const char *s1);
char				*pf_strjoin(char const *s1, char const *s2, int f);
char				*pf_strnew(size_t size, int c);
char				*pf_substr(char const *s, unsigned int start, size_t len);

#endif
