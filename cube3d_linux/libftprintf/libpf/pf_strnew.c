#include "../includes/libpf.h"

char	*pf_strnew(size_t size, int c)
{
	char *ret;

	if (!(ret = malloc(sizeof(char) * size + 1)))
		return (NULL);
	pf_memset(ret, c, size);
	ret[size] = '\0';
	return (ret);
}
