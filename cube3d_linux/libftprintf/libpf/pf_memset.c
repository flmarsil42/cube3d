#include "../includes/libpf.h"

void	*pf_memset(void *b, int c, size_t len)
{
	size_t i;

	i = -1;
	while (len--)
		((unsigned char *)b)[++i] = (unsigned char)c;
	return (b);
}
