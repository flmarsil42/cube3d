#include "../includes/libpf.h"

char	*pf_substr(char const *s, unsigned int start, size_t len)
{
	char	*ret;
	size_t	i;
	size_t	j;

	if (!s || !(ret = malloc(sizeof(char) * len + 1)))
		return (NULL);
	if (start > pf_strlen(s))
		return (pf_calloc(1, sizeof(char)));
	i = start;
	j = 0;
	while (j < len && s[i])
		ret[j++] = s[i++];
	ret[j] = '\0';
	return (ret);
}
