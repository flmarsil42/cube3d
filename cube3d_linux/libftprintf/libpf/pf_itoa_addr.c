#include "../includes/libpf.h"

char	*pf_itoa_addr(long value, char *base)
{
	char			*tab;
	int				cpt;
	unsigned long	tmp;

	cpt = 3;
	tmp = value;
	while (tmp >= 16 && (tmp /= 16))
		++cpt;
	tmp = value;
	if (!(tab = (char *)malloc(sizeof(char) * cpt + 1)))
		return (NULL);
	tab[cpt] = '\0';
	while (tmp >= 16)
	{
		tab[--cpt] = base[tmp % 16];
		tmp /= 16;
	}
	tab[--cpt] = base[tmp % 16];
	tab[0] = '0';
	tab[1] = 'x';
	return (tab);
}
