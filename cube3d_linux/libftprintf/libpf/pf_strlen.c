#include "../includes/libpf.h"

size_t	pf_strlen(const char *str)
{
	size_t i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}
