#include "../includes/libpf.h"

void	*pf_calloc(size_t count, size_t size)
{
	void	*ret;

	if (!(ret = malloc(size * count)))
		return (NULL);
	if (!count || !size)
		return (ret);
	pf_memset(ret, 0, count * size);
	return (ret);
}
