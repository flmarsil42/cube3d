#include "../includes/libpf.h"

void	pf_putchar_fd(char c, int fd)
{
	write(fd, &c, 1);
}
