#include "../includes/libpf.h"

char	*pf_strchr(const char *s, int c)
{
	while ((*s && *s != (char)c))
		s++;
	if (*s && *s == (char)c)
		return ((char *)s);
	return (NULL);
}
