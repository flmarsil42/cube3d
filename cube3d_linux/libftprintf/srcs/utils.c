#include "../includes/ft_printf.h"

char	*convert_x(va_list av, char c, t_elem *e)
{
	unsigned int	n;
	long			p;
	char			*base;
	char			*ret;

	base = (c == 'X') ? "0123456789ABCDEF" : "0123456789abcdef";
	if (c == 'x' || c == 'X')
	{
		n = va_arg(av, unsigned int);
		if (!(ret = pf_itoa_base(n, base)))
			return (NULL);
	}
	else
	{
		p = va_arg(av, long);
		if (p == 0 && e->preci == 0)
		{
			if (!(ret = pf_strdup("0x")))
				return (NULL);
		}
		else if (!(ret = pf_itoa_addr(p, base)))
			return (NULL);
	}
	return (ret);
}

void	init_elem(t_elem *e)
{
	e->ret = NULL;
	e->flag = 0;
	e->width = 0;
	e->preci = -1;
	e->type = 0;
	e->r_neg = 0;
	e->p_neg = 0;
	e->w_neg = 0;
	e->zero = 0;
	e->c = 0;
	e->i = e->i;
	e->count = e->count;
	e->final_len = e->final_len;
}

void	ft_except(t_elem *e)
{
	((e->preci == 0) && e->ret[0] == '0' && e->type != 'p')
		? e->ret[0] = '\0' : 0;
	if (e->p_neg == 1 && e->flag != -1 && e->type != 'u')
		e->preci = (e->r_neg == 1) ? e->width - 1 : e->width;
}

void	final_conversion(t_elem *e)
{
	(e->preci >= 0 && e->flag == 1 && pf_strchr("diuxX", e->type))
		? e->flag = 0 : 0;
	(e->w_neg == 1) ? e->flag = -1 : 0;
	(e->w_neg == 1) ? e->width *= -1 : 0;
	(e->flag == 1) ? e->zero = 1 : 0;
	(e->preci >= 0 && e->type != '%') ? e->zero = 2 : 0;
	(e->preci > 0 && e->width == 0 && e->type != '%' && e->type != 's')
		? e->width = e->preci : 0;
	(pf_strchr("diuxXp%%", e->type)) ? ft_except(e) : 0;
}

int		fill_elem(va_list av, t_elem *e, char const *s)
{
	int x;

	check_flag(e, s);
	check_width(av, e, s);
	check_precision(av, e, s);
	x = check_type(av, e, s);
	return (x);
}
